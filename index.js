// a proxy for a lfs server that doesnt have any locking implemented.

//TODO: needs a lot of cleanup and I don't think the proxy of large file transfer is working

const http = require('http');
const url = require('url');
const uuid = require('uuid/v4');
const express = require('express');
const app = express();

var fs = require('fs');
var path = require('path');

var bodyParser = require('body-parser');

app.use(bodyParser.json({ type: 'application/vnd.git-lfs+json' }))
//app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({extended: true}));
//app.use(bodyParser.json({limit:'50mb'}));

const basePath = 'lock_store/';
const lockFileExt = '.lock';

// https://github.com/git-lfs/git-lfs/blob/master/docs/api/locking.md
app.post('*/lfs/locks', (request, response) =>
{
    var auth = request.headers['authorization'];
    /*console.log("Authorization Header is: ", auth);
    console.log("<<<<<<<<<<<<<<>>>>>>>> START");
    console.log('lfs locks request!!');
    console.log("host = %s", request.headers['host']);
    console.log("url = %s", request.url);
    console.log("method = %s", request.method);
    console.log(`HEADERS: ${JSON.stringify(request.headers)}`);*/
   
    if (!auth) 
    {
        response.statusCode = 401;
        response.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
        response.write("need authentication");
        request.pipe(response);
    }
    else
    {
        var tmp = auth.split(' ');
        var buf = new Buffer(tmp[1], 'base64');
        var plain_auth = buf.toString();
        
        //console.log("Decoded Authorization ", plain_auth);
        // At this point plain_auth = "username:password"
        var creds = plain_auth.split(':');      // split on a ':'
        var username = creds[0];
        //var password = creds[1];
        //console.log('USERNAME: %s', username);
        // {
        //   "path": "foo/bar.zip",
        //   "ref": {
        //     "name": "refs/heads/my-feature"
        //   }
        // }
        //console.log(request.body);
        if (request.body.path != null)
        {
            const fileName = request.body.path;
            var filePath = basePath + fileName + lockFileExt;
            //console.log('file path: %s', request.body.path);

            var dirName = path.dirname(filePath);
            if (!fs.existsSync(dirName))
            {
                fs.mkdirSync(dirName)
            }

            if (fs.existsSync(filePath) )
            {
                //console.log("LOCK ALREADY EXISTS");
                var lockUser = 'none';
                var lockId = 'none';
                fs.readFile(filePath, function read(err, data) 
                {
                    if (err) 
                    {
                        throw err;
                    }
                    //console.log(data);
                    var dataString = data.toString();
                    var pair = dataString.split(':');
                    lockUser = pair[0];
                    lockId = pair[1];
                    response.writeHead(409, 
                    {
                        'content-type' : 'application/vnd.git-lfs+json',
                        'X-Powered-By': 'bacon'
                    });

                    var json = JSON.stringify({
                        'lock' : { id : 'lockUser' },
                        'message' : 'lock already created',
                        //'documentation_url' : "https://www.google.com/search?&q=lfs+error",
                        //'request_id' :
                    });

                    response.write(json);
                    request.pipe(response);
                });
            }
            else 
            {
                var id = uuid();
                var lockFileName = basePath + id;
                //console.log("CREATING LOCK id= %s", id);
                {
                    var lockStream = fs.createWriteStream(lockFileName);
                    lockStream.once('open', function(fd) 
                    {
                        lockStream.write(fileName);
                        lockStream.end();
                    });
                }
           
                //todo: test write file for locks
                var stream = fs.createWriteStream(filePath);
                stream.once('open', function(fd) 
                {
                    stream.write(username);
                    stream.write(':');
                    stream.write(id);
                    stream.end();
                });
                response.writeHead(201, 
                {
                    'content-type' : 'application/vnd.git-lfs+json',
                    'X-Powered-By': 'bacon'
                });

                var date = new Date();
                var lockTime = date.toISOString();

                var json = JSON.stringify({
                    'id' : id,
                    'path' : fileName,
                    'locked_at' : lockTime,
                    'owner' :
                    {
                        'name' : username
                    }
                });

                response.write(json);
                request.pipe(response);
            }
        }
        else
        {
            response.writeHead(501, '');
            response.write("blah");
            request.pipe(response);
        }
    }
    
})
.get('*/lfs/locks', (request, response) =>
{
    /*console.log("GET locks >>>>>>>>>>>>>");
    console.log("host = %s", request.headers['host']);
    console.log("url = %s", request.url);
    console.log(`HEADERS: ${JSON.stringify(request.headers)}`);*/
    var parts = url.parse(request.url, true);
    var query = parts.query;
    if (query && query.path) 
    {
        var fileName = query.path;
        //console.log('file name= %s', fileName);
        var filePath = basePath + fileName + lockFileExt;
        //console.log('full file path: %s', filePath);

        if (fs.existsSync(filePath) )
        {
            var lockUser = 'none';
            var lockId = 'none';

            fs.readFile(filePath, function read(err, data) 
            {
                if (err) 
                {
                    throw err;
                }
                var dataString = data.toString();
                var pair = dataString.split(':');
                lockUser = pair[0];
                lockId = pair[1];

                //TODO: shared write locks
                response.writeHead(200, 
                {
                    'content-type' : 'application/vnd.git-lfs+json',
                    'X-Powered-By': 'bacon'
                });

                //TODO: store date with lock files....
                var date = new Date();
                var lockTime = date.toISOString();
                var json = JSON.stringify(
                {
                    'locks' : 
                    [
                        {
                            'id' : lockId,
                            'path' : fileName,
                            'locked_at' : lockTime,
                            'owner' :
                            {
                                'name' : lockUser
                            }
                        }
                    ]
                });
                response.write(json);
                request.pipe(response);
            });
        }
        else 
        {
            response.writeHead(200, 
            {
                'content-type' : 'application/vnd.git-lfs+json',
                'X-Powered-By': 'bacon'
            });

            var json = JSON.stringify(
            {
                'locks' : []
            });

            response.write(json);
            request.pipe(response);
        }
    }
    else 
    {
        //console.log('FILE DOESNT EXIST');
        response.writeHead(501, '');
        response.write("not implemented");
        request.pipe(response);
    }
    //request.pipe(response);
})
.post('*/:id/unlock', (request, response) =>
{
    //console.log(request.params.id);
    var uid = request.params.id;
    
    var auth = request.headers['authorization'];
    /*console.log("Authorization Header is: ", auth);
    console.log("host = %s", request.headers['host']);
    console.log("url = %s", request.url);
    console.log("method = %s", request.method);
    console.log(`HEADERS: ${JSON.stringify(request.headers)}`);*/
   
    if (!auth) 
    {
        response.statusCode = 401;
        response.setHeader('WWW-Authenticate', 'Basic realm="Secure Area"');
        response.write("need authentication");
        request.pipe(response);
    }
    else
    {
        var tmp = auth.split(' ');              // Split on a space, the original auth looks like  "Basic Y2hhcmxlczoxMjM0NQ==" and we need the 2nd part
        var buf = new Buffer(tmp[1], 'base64'); // create a buffer and tell it the data coming in is base64
        var plain_auth = buf.toString();        // read it back out as a string
        // At this point plain_auth = "username:password"
        var creds = plain_auth.split(':');      // split on a ':'
        var username = creds[0];

        var idFileName = uid;
        //console.log('id file name= %s', idFileName);
        
        var idFilePath = basePath + idFileName;
        //console.log('full file path: %s', idFilePath);

        var fileName = '';
        
        var forceDelete = false;
        if (request.body.force != null)
        {
            forceDelete = request.body.force;
        }

        if (fs.existsSync(idFilePath) )
        {
            var lockUser = 'none';
            var lockId = 'none';

            fs.readFile(idFilePath, function read(err, data)
            {
                if (err) 
                {
                    throw err;
                }
                //console.log(data);

                fileName = data.toString();
                var filePath = basePath + fileName + lockFileExt;
                //console.log('file path: %s', request.body.path);

                if (fs.existsSync(filePath) )
                {
                    //console.log("LOCK file EXISTS");
                    fs.readFile(filePath, function read(err, data) 
                    {
                        if (err) 
                        {
                            throw err;
                        }
                        //console.log(data);
                        var dataString = data.toString();
                        var pair = dataString.split(':');
                        lockUser = pair[0];
                        lockId = pair[1];
                        if (lockUser != username && !forceDelete)
                        {
                            response.writeHead(403, 
                            {
                                'content-type' : 'application/vnd.git-lfs+json',
                                'X-Powered-By': 'bacon'
                            });

                            var json = JSON.stringify(
                            {
                                'message' : "not allowed to delete this lock",
                            });
                            response.write(json);
                            request.pipe(response);
                        }
                        else 
                        {
                            //tODO: should we not delete the id file?
                            fs.unlinkSync(idFilePath);
                            fs.unlinkSync(filePath);
                            response.writeHead(200, 
                            {
                                'content-type' : 'application/vnd.git-lfs+json',
                                'X-Powered-By': 'bacon'
                            });

                            //TODO: store time
                            var date = new Date();
                            var lockTime = date.toISOString();
                            var json = JSON.stringify(
                            {
                                'lock' : 
                                {
                                    'id' : uid,
                                    'path' : fileName,
                                    'locked_at' : lockTime,
                                    'owner' :
                                    {
                                        'name' : username
                                    }
                                }
                            });
                            response.write(json);
                            request.pipe(response);
                        }
                    });
                }

            });
        }       
    }
})
.all('*', (request, response) =>
{
    //TODO: configurable proxy settings...
//    console.log("trying somethign else..............proxy.>");
    var proxyRequest = http.request(
    {
        //host: request.headers['host'],
        host: 'vgd-bm01',
        port: 8080,
        path: request.url,
        method: request.method,
        headers: request.headers
    }, 
    function(proxyResponse) 
    {
        /*console.log("writing>>>>>>>>>>>>>");
        console.log("host = %s", proxyResponse.headers['host']);
        console.log("url = %s", proxyResponse.url);
        console.log("method = %s", proxyResponse.method);
        console.log(`STATUS: ${proxyResponse.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(proxyResponse.headers)}`);*/
        //response.setDefaultEncoding()
        
        proxyResponse.on("data", (chunk) => 
        {
            console.log("on data ");
            console.log(chunk);
            response.write(chunk);
        });

        proxyResponse.on('close', () =>
        {
            // closed, let's end client request as well 
            response.writeHead(proxyResponse.statusCode, proxyResponse.headers);
            //res.writeHead(cres.statusCode);
            response.end();
        });

        proxyResponse.on('end', () =>
        {
            // finished, let's finish client request as well 
            response.writeHead(proxyResponse.statusCode, proxyResponse.headers);
            response.end();
        });
        //response.writeHead(proxyResponse.statusCode, proxyResponse.headers);
        //response.write(proxyResponse.data);
        //proxyResponse.pipe(response);
        //console.log(proxyResponse);
        //console.log(proxyRequest);
    }).on('error', (e)=>
    {
        console.log("ERROR");
        console.log(e.message);
        response.writeHead(500);
        response.end();
    });
    request.pipe(proxyRequest);	
})
.listen(1337, (err) =>
{
    if (err) 
    {
        console.log('error');
    }

    console.log('listening');

});